﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace de_duplication
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void MainPage1_Loaded(object sender, RoutedEventArgs e)
        {
            //DirectoryInfo folder = new DirectoryInfo(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName));
            DirectoryInfo folder = new DirectoryInfo(@"c:\");
            FileInfo[] images = folder.GetFiles("*.jpg");
            foreach (FileInfo img in images)
            {
                //Thumbnails.Items.Add(img.FullName);
                listView.Items.Add(new Windows.UI.Xaml.Media.Imaging.BitmapImage(new Uri(img.FullName)));
            }
        }

        //http://stackoverflow.com/questions/1708239/how-can-i-load-a-folders-files-into-a-listview
        private void GetFiles()
        {
            // here I had to manually add System.Windows.Forms reference to the project
            // but so far I couldn't do this for UniversalWindows app...
            var folderPicker = new System.Windows.Forms.FolderBrowserDialog();
            if (folderPicker.ShowDialog() == DialogResult.OK)
            {

                listView.Items.Clear();

                string[] files = Directory.GetFiles(folderPicker.SelectedPath);
                foreach (string file in files)
                {

                    string fileName = Path.GetFileNameWithoutExtension(file);
                    ListViewItem item = new ListViewItem(fileName);
                    item.Tag = file;

                    listView.Items.Add(item);

                }
            }
        }
    }
}
